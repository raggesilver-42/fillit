/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetrimino_aux.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/01 18:23:08 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/05/08 11:14:47 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <fcntl.h>

void	tetr_reset(t_list *tetr)
{
	T_TETR(tetr->content)->used = 0;
}

t_list	*get_tetriminoes(const char *f)
{
	int		fd;
	char	nl;
	t_list	*tetriminoes;
	t_tetr	*aux;

	if ((fd = open(f, O_RDONLY)) < 0)
		EXIT_ERROR(INVALID_FD);
	tetriminoes = NULL;
	while ((aux = tetr_new_from_fd(fd)))
	{
		if (tetriminoes)
			ft_lstappend(tetriminoes, aux, sizeof(*aux));
		else
			tetriminoes = ft_lstnew(aux, sizeof(*aux));
		nl = 0;
		ft_memdel((void **)&aux);
		if ((nl += read(fd, &nl, 1)) != ('\n' + 1) && nl > 0)
			EXIT_ERROR(INVALID_TETRIMINO);
		else if (nl == 0)
			break ;
	}
	close(fd);
	return (tetriminoes);
}
