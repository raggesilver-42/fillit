/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 12:43:32 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/05/01 18:27:08 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

t_map		*map_new(size_t size)
{
	size_t	y;
	t_map	*res;

	res = (t_map *)malloc(sizeof(*res));
	res->size = size;
	res->str = malloc(sizeof(*(res->str)) * size);
	y = -1;
	while (++y < size)
	{
		res->str[y] = ft_strnew(size);
		res->str[y] = ft_memset(res->str[y], '.', size);
	}
	T_POINT_ZERO(res->last);
	return (res);
}

void		map_destroy(t_map **map)
{
	int i;

	if (!map || !*map)
		return ;
	i = -1;
	while (++i < (*map)->size)
		ft_strdel(&(*map)->str[i]);
	ft_memdel((void **)&(*map)->str);
	ft_memdel((void **)map);
	*map = NULL;
}

void		print_map(t_map *map)
{
	int i;

	i = -1;
	while (++i < map->size)
	{
		ft_putstr(map->str[i]);
		ft_putchar('\n');
	}
}
