/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 14:13:27 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/05/02 00:44:14 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		place_tetr(t_tetr *tt, t_map *map, int do_place)
{
	t_point	ts;
	t_point	comp;
	t_point cord;
	int		ct;

	ct = -1;
	while (++ct < 4)
	{
		ts = tt->cords[ct];
		if (ct == 0)
			T_POINT_SET(comp, MIN(ts.x, tt->offset.x), ts.y);
		cord.x = map->last.x + ts.x - comp.x;
		cord.y = map->last.y + ts.y - comp.y;
		if (map->str[cord.y][cord.x] != '.')
			return (0);
		else if (do_place)
			map->str[cord.y][cord.x] = tt->id;
	}
	tt->used = do_place;
	return ((do_place) ? 1 : place_tetr(tt, map, 1));
}

void	remove_tetr(t_tetr *tt, t_map *map)
{
	int	x;
	int	y;

	y = -1;
	while (++y < map->size)
	{
		x = -1;
		while (++x < map->size)
		{
			if (map->str[y][x] == tt->id)
				map->str[y][x] = '.';
		}
	}
	tt->used = 0;
}

t_map	*do_magic(t_list *tetriminoes, t_map *map)
{
	t_point	p;
	t_tetr	*tt;

	T_POINT_ZERO(p);
	RETURN_VAL_IF_FAIL(map, (tetriminoes));
	tt = T_TETR(tetriminoes->content);
	while (p.y < map->size)
	{
		map->last = p;
		if (map->size - p.y >= tt->height && map->size - p.x >= tt->width &&
			place_tetr(tt, map, 0))
		{
			if (do_magic(tetriminoes->next, map))
				return (map);
			else
				remove_tetr(tt, map);
		}
		if ((p.x = (p.x + 1) % map->size) == 0)
			p.y++;
	}
	return (NULL);
}

int		fillit(const char *f)
{
	t_list	*tetriminoes;
	t_map	*map;
	size_t	size;

	tetriminoes = get_tetriminoes(f);
	map = map_new(ft_sqrt_ceil(ft_lstcnt(tetriminoes) * 4));
	while (!do_magic(tetriminoes, map))
	{
		size = map->size + 1;
		map_destroy(&map);
		map = map_new(size);
		ft_lstiter(tetriminoes, &tetr_reset);
	}
	print_map(map);
	map_destroy(&map);
	ft_lstdel(&tetriminoes, &tetr_destroy);
	return (0);
}

int		main(int argc, char **argv)
{
	if (argc == 2)
		return (fillit(argv[1]));
	else
	{
		ft_putstr("Usage: ./fillit <tetriminoes_file>\n");
		return (-1);
	}
}
