# Name here
NAME=fillit
REC=true
CC=gcc
FLAGS=-Wall -Werror -Wextra

# Sources here
SRC=fillit.c \
	tetrimino.c \
	tetrimino_aux.c \
	map.c
OBJ=$(SRC:.c=.o)

# Dependencies here (.a files)
# Make sure those dependencies' directories are on your project's root
# i.e: dep = libft.a
# tree: . .. libft/ Makefile #...
DEP=libft.a
DDEP=$(DEP:.a=)
FDEP=$(foreach DP, $(DDEP), -L$(DP) -l$(subst lib,,$(DP)))
DEPS=$(foreach DP, $(DEP), $(DP:.a=)/$(DP))

INCS=$(foreach lib, $(DDEP), -I $(lib)/includes)

.PHONY: $(NAME)

all: $(NAME)

$(NAME):
	$(foreach dep, $(DDEP), make -C $(dep))
	$(CC) $(FLAGS) $(SRC) $(DEPS) -o $@ $(INCS) $(FDEP)

clean:
ifdef REC
	@$(foreach dep, $(DDEP), make -C $(dep) clean)
endif
	rm -f $(OBJ)

fclean: clean
ifdef REC
		@$(foreach dep, $(DDEP), make -C $(dep) fclean)
endif
	rm -f $(NAME)

re: fclean all

run: $(NAME)
	./$(NAME) list.tetr

debug: FLAGS += -g
debug: fclean all

TEST_ARGS=$(shell find tests -name "*.list" -not -name "test*")
TEST_FLAGS=--error-exitcode=1 --leak-check=full

ci:
	@make REC=true re
	$(foreach T, $(TEST_ARGS), valgrind $(TEST_FLAGS) ./$(NAME) $(T);)

