/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 18:16:53 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/02/20 18:21:17 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list *cur;
	t_list *nxt;

	if (alst && *alst)
	{
		cur = *alst;
		while (cur)
		{
			del(cur->content, cur->content_size);
			nxt = cur->next;
			free(cur);
			cur = nxt;
		}
		*alst = NULL;
	}
}
