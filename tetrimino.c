/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetrimino.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 14:47:27 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/05/08 23:53:59 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	tetr_has_neighbor(t_tetr *tt, int x, int y)
{
	int	i;

	i = -1;
	while (++i < 4)
	{
		if (tt->cords[i].x == x && tt->cords[i].y == y)
			return (1);
	}
	return (0);
}

static void	tetr_validate(t_tetr *tt)
{
	int		connections;
	int		i;
	t_point p;

	i = -1;
	connections = 0;
	while (++i < 4)
	{
		p = tt->cords[i];
		if (p.x > 0 && tetr_has_neighbor(tt, p.x - 1, p.y))
			connections++;
		if (p.x < 3 && tetr_has_neighbor(tt, p.x + 1, p.y))
			connections++;
		if (p.y < 3 && tetr_has_neighbor(tt, p.x, p.y + 1))
			connections++;
		if (p.y > 0 && tetr_has_neighbor(tt, p.x, p.y - 1))
			connections++;
		T_POINT_SET(tt->offset, MIN(tt->offset.x, p.x), MIN(tt->offset.y, p.y));
		tt->width = MAX(tt->width, (p.x - tt->offset.x + 1));
		tt->height = MAX(tt->height, (p.y - tt->offset.y + 1));
	}
	if (connections != 6 && connections != 8)
		EXIT_ERROR(INVALID_TETRIMINO);
}

t_tetr		*tetr_new(char *str)
{
	t_tetr		*res;
	static int	id;
	int			i;
	t_point		p;

	RETURN_VAL_IF_FAIL(NULL, (res = malloc(sizeof(*res))));
	res->id = 'A' + id++;
	res->cords = malloc(sizeof(*res->cords) * 4);
	T_POINT_ZERO(p);
	i = 0;
	while (*str)
	{
		if (*str == '#')
			res->cords[i++] = p;
		else if (*str != '.')
			EXIT_ERROR(INVALID_TETRIMINO);
		p.x = (p.x + 1) % 4;
		p.y = (p.x == 0) ? p.y + 1 : p.y;
		str++;
	}
	T_POINT_SET(res->offset, 4, 4);
	res->width = 0;
	res->height = 0;
	tetr_validate(res);
	return (res);
}

t_tetr		*tetr_new_from_fd(int fd)
{
	char	*tmp;
	char	ln[6];
	int		i;
	int		aux;
	t_tetr	*res;

	i = 0;
	ft_bzero(ln, 6);
	tmp = ft_strnew(16);
	while (i < 4 && ((aux = read(fd, ln, 5)) == 5))
	{
		if (ln - ft_strchr(ln, '\n') != -4 && aux != 4)
			EXIT_ERROR(INVALID_TETRIMINO);
		ln[4] = 0;
		ft_strcat(tmp, ln);
		i++;
	}
	if (i < 4 || ft_strchrcnt(tmp, '#') != 4)
		EXIT_ERROR(INVALID_TETRIMINO);
	res = tetr_new(tmp);
	ft_strdel(&tmp);
	return (res);
}

void		tetr_destroy(void *t, size_t sz)
{
	(void)sz;
	free(T_TETR(t)->cords);
	free(t);
}
