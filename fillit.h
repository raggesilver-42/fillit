/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 14:51:14 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/05/02 00:41:34 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include "libft.h"
# include <unistd.h>

# define T_POINT_ZERO(p) ({ p.x = 0; p.y = 0; })
# define T_POINT_COPY(p1, p2) ({ p2.x = p1.x; p2.y = p1.y; })
# define T_POINT_CLONE(p) ({ t_point res; res.x = p.x; res.y = p.y; res; })
# define T_POINT_SET(p, _x, _y) ({ p.x = _x; p.y = _y; })

typedef struct			s_point
{
	int					x;
	int					y;
}						t_point;

# define EXIT_ERROR(err) ({ ft_putstr(err##_STR); exit(err##_ID); })

# define INVALID_FD_ID 5
# define INVALID_FD_STR "error\n"

# define T_MAP(x) ((t_map *)x)

typedef struct			s_map
{
	int					size;
	t_point				last;
	char				**str;
}						t_map;

t_map					*map_new(size_t size);
void					map_destroy(t_map **map);
void					print_map(t_map *map);

# define MIN(a, b) ((a < b) ? a : b)
# define MAX(a, b) ((a > b) ? a : b)

# define INVALID_TETRIMINO_ID 2
# define INVALID_TETRIMINO_STR "error\n"

# define T_TETR(x) ((t_tetr *)x)

typedef struct			s_tetr
{
	t_point				*cords;
	int					width;
	int					height;
	t_point				offset;
	char				id;
	int					used;
}						t_tetr;

t_tetr					*tetr_new(char *str);
t_tetr					*tetr_new_from_fd(int fd);
void					tetr_destroy(void *t, size_t sz);

void					tetr_reset(t_list *tetr);
t_list					*get_tetriminoes(const char *f);

#endif
